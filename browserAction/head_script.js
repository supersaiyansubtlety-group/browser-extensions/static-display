import * as c from '/common.js';
// set width of popup according to options
c.getOptions(options => {
  document.body.style.width = options[c.OptionKeys.width];
  console.log('set document.body.style.width to: ', options[c.OptionKeys.width]);
});