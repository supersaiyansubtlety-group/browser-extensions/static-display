import * as c from '/common.js';

window.addEventListener('beforeunload', saveDisplay);
const form = document.getElementById('display_form')
form.addEventListener('change', saveDisplay);
// set width of popup according to options
c.getOptions(options => {
  document.body.style.width = options[c.OptionKeys.width];
  console.log('set document.body.style.width to: ', options[c.OptionKeys.width]);
});


// init form to display values
c.getDisplay(display => {
  if (display.title !== undefined) 
    document.getElementById(c.TITLE_KEY).value = display.title;
  
  if (display.description !== undefined) 
    document.getElementById(c.DESCRIPTION_KEY).value = display.description;
});

function saveDisplay() {
  const display = {
    title: getElementValue(c.TITLE_KEY),
    description: getElementValue(c.DESCRIPTION_KEY)
  };
  c.store(
    c.DISPLAY_KEY, 
    display
  );
  c.updateDisplay(display);
}

function getElementValue(id) {
  let value = document.getElementById(id).value;
  return typeof value === 'string' && value !== '' ? 
    value : undefined;
}

function onFormSumbit(submitted) {
  submitted.preventDefault();
  saveDisplay();
}