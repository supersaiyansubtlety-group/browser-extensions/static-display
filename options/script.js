import * as c from '/common.js';

const BADGE_ELEM = document.getElementById('show_badge_choice');
const WIDTH_ELEM = document.getElementById('popup_width_choice');

document.getElementById('options_form').addEventListener('change', onOptionsChange);
document.getElementById('reset_button').addEventListener('click', confirmReset);

function confirmReset() {
  const confirmed = confirm("Are you sure you want to reset all options?");
  if (confirmed) {
    const options = c.resetOptions();
    c.updateDisplay(undefined, options);
    populateOptions(options);
  }
}

const tooltipChoiceElemIds = {
  [c.TooltipDisplay.title]: 'tooltip_title_choice',
  [c.TooltipDisplay.description]: 'tooltip_description_choice'
};

c.biMapIfy(tooltipChoiceElemIds);
Object.freeze(tooltipChoiceElemIds);

const titleChoiceElemIds = {
  [c.PageTitleDisplay.none]: 'leave_title_choice',
  [c.PageTitleDisplay.prepend]: 'prepend_to_title_choice',
  [c.PageTitleDisplay.append]: 'append_to_title_choice'
};
c.biMapIfy(titleChoiceElemIds);
Object.freeze(titleChoiceElemIds);

populateOptions();

function populateOptions(options) {
  if (options !== undefined) populateOptionsImpl(options);
  else  c.getOptions(populateOptionsImpl);
}

function populateOptionsImpl(options) {
  BADGE_ELEM.checked = options[c.OptionKeys.badge];
  populateRadioChoice(c.OptionKeys.tooltip, options, tooltipChoiceElemIds);
  populateRadioChoice(c.OptionKeys.title, options, titleChoiceElemIds);
  WIDTH_ELEM.value = options[c.OptionKeys.width];
}

function getTooltipElemChoice(elem) {
  if (Object.is(elem, TOOLTIP_NONE_ELEM)) return c.TooltipDisplay.none;
  else if (Object.is(elem, TOOLTIP_TITLE_ELEM)) return c.TooltipDisplay.title;
  else if (Object.is(elem, TOOLTIP_DESCRIPTION_ELEM)) return c.TooltipDisplay.description;
  else c.logError('Received element that does not map to tooltip choice: ', elem);
}

function onOptionsChange(event) {
  const newOptions = { };
  switch (event.srcElement.name) {
    case 'badge_option':
      newOptions[c.OptionKeys.badge] = BADGE_ELEM.checked;
      break;
    case 'tooltip_option':
      newOptions[c.OptionKeys.tooltip] = tooltipChoiceElemIds.getKey(event.srcElement.id);
      break;
    case 'title_option':
      newOptions[c.OptionKeys.title] = titleChoiceElemIds.getKey(event.srcElement.id);
      break;
    case 'width_option':
      newOptions[c.OptionKeys.width] = WIDTH_ELEM.value;
      break;
    default: 
      c.logError('Unrecognized name for form change: ', event.srcElement.name);
      break;
  }
  saveOptions(newOptions);
}

function saveOptions(newOptions) {
  c.getOptions(options => {
    let changed = false;
    for (let [key, value] of Object.entries(newOptions)) {
      if (options[key] !== value) {
        changed = true;
        options[key] = value;
      }
    }
    
    if (changed) {
      c.store(c.OPTIONS_KEY, options);
      c.updateDisplay(undefined, options);
    }
  });
}

function populateRadioChoice(optionsKey, options, choiceElemIds) {
  const chosen = options[optionsKey];
  const choiceElemId = choiceElemIds[chosen];
  const choiceElem = document.getElementById(choiceElemIds[chosen]);
  if(!choiceElem) {
    c.logError('!choiceElem');
    console.log('chosen: ', chosen);
    console.log('choiceElemId: ', choiceElemId);
  }
  if (chosen === undefined) c.logError('Received unrecognized option for ' + optionsKey + ': ', chosen);
  else choiceElem.checked = true;
}