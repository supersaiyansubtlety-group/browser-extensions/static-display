export const TITLE_KEY = 'title';
export const DESCRIPTION_KEY = 'description';
export const DISPLAY_KEY = 'display';
export const OPTIONS_KEY = 'options';

export const OptionKeys = {
  badge: null,
  tooltip: null,
  title: null,
  width: null
}
enumIfy(OptionKeys);

export const TooltipDisplay = {
  title: null,
  description: null
}
enumIfy(TooltipDisplay);

export const PageTitleDisplay = {
  none: null,
  prepend: null,
  append: null
}
enumIfy(PageTitleDisplay);

export const DEFAULT_OPTIONS = {};
DEFAULT_OPTIONS[OptionKeys.badge] = true;
DEFAULT_OPTIONS[OptionKeys.tooltip] = TooltipDisplay.description;
DEFAULT_OPTIONS[OptionKeys.title] = PageTitleDisplay.none;
DEFAULT_OPTIONS[OptionKeys.width] = 150;
Object.freeze(DEFAULT_OPTIONS);

export function updateDisplay(display, options) {
  let update = _display => {
    updateDisplayImpl(_display.title, _display.description, options);
  };
  if (display === undefined) getDisplay(update);
  else update(display);
}

function updateDisplayImpl(title, description, options) {
  if (title === undefined) title = null;
  if (description === undefined) description = null;
  let update = _options => {
    updateBadge(_options[OptionKeys.badge], title);
    updateTooltip(_options[OptionKeys.tooltip], title, description);
    // TODO: page title option
  };
  if (options === undefined) getOptions(update);
  else update(options);
}

function updateBadge(badgeOption, title) {
  if (badgeOption === true) setBadge(title);
  else if (badgeOption === false) setBadge(null);
  else logError('Badge option is not boolean: ', badgeOption);
}

function updateTooltip(tooltipOption, title, description) {
  let tooltip;
  switch (tooltipOption) {
    case TooltipDisplay.none:
      tooltip = null;
      break;
    case TooltipDisplay.title: 
      tooltip = title;
      break;
    case TooltipDisplay.description:
      tooltip = description;
      break;
    default: 
      logError('Got unrecognized tooltip option: ', tooltipOption);
      return;
  }
  
  setTooltip(tooltip);
}

function setBadge(string) {
  browser.browserAction.setBadgeText({ text: string });
}

function setTooltip(string) {
  browser.browserAction.setTitle({ title: string });
}

export function getDisplay(consumeDisplay) {
  browser.storage.local.get(DISPLAY_KEY).then(
    display => {
      if (display !== undefined && typeof display[DISPLAY_KEY] === 'object') consumeDisplay(display[DISPLAY_KEY]);
      else consumeDisplay({ });
    },
    logError
  );
}

export function getOptions(consumeOptions) {
  browser.storage.local.get(OPTIONS_KEY).then(
    options => {      
      let storeOptions = false;
      if (options === undefined || options[OPTIONS_KEY] === undefined) {
        options = { };
        storeOptions = true;
      } else options = options[OPTIONS_KEY];
      
      storeOptions |=  validateOptions(options);      
      if (storeOptions) store(OPTIONS_KEY, options);
      consumeOptions(options);
    },
    logError
  );
}

export function resetOptions() {
  store(OPTIONS_KEY, DEFAULT_OPTIONS);
  return DEFAULT_OPTIONS;
}

function validateOptions(options) {
  let changed = false;
  for (let key of Object.keys(OptionKeys))
    if (options[key] === undefined) {
      options[key] = DEFAULT_OPTIONS[key];
      changed = true;
  }
  return changed;
}

export function store(key, value) {
  const saveObj = {};
  saveObj[key] = value;
  return browser.storage.local.set(saveObj);
}

export function logError(error) {
  console.error('Error: ', error);
}

export function logStringified() {
  let staticElements = [];
  for (let arg of arguments) staticElements.push(JSON.stringify(arg));
  console.log(staticElements);
}

export function doNothing() { }

export function enumIfy(obj) {
  for (let key of Object.keys(obj)) obj[key] = key;
  Object.freeze(obj);
}

export function biMapIfy(map) {
  let reverseMap = {};
  for (let [key, value] of Object.entries(map))
    reverseMap[value] = key;
  map.reverseMap = reverseMap;
  map.getKey = value => map.reverseMap[value];
}